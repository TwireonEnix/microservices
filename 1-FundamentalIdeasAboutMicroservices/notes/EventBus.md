# Event Buses

There are many implementations available for buses, RabbitMQ, Kafka, NATS, etc.

This buses receive events and publishes them to listeners. The events does not have to be of a
certain structure They're just things. Like objects, json structure, raw data, a single string, etc.

All of these have many different subtle features that make async communication way easier or way
harder.

First event bus will be a simple version with express. However, it will not implement the vast
majority of features a normal bus has.
