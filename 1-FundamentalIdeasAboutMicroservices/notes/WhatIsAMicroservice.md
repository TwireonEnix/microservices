# What is a Microservice?

A monolithic app contains

routing | middleware | business-logic | database-access

to implement all features of an app.

A single microservice contains routine middleware business-logic database-access to implement **_One
Single Feature_**

> Each microservice works with separate database.

# Challanges

## Data Management between services

This is the big problem of microservices and what all the focus is in several courses.

Because each microservice uses one database (if it uses it), the great obstacle for using
microservices is the data synchronization.

With microservices we store and access data in strange ways.

> Each service get its own database (if it needs one).

### Accessing Data

> Services will never, ever reach into another services database.

This pattern is called **Database-Per-Service.**

This nevers happens because

- We want each service to run independently of other services
- Database schema / structure might change unexpectedly
- Some services might function more efficiently with different types of DB's (sql vs nosql)

Every successful microservices implementation follow this pattern.

Then the problem is: What if we need to create a service that consumes or depends on data created
distributed over the databases of n more services?

# Sync Communication Between Services.

There are two communication strategies between services _Sync_ and _Async_ (These do not refer to
programming patterns in js).

- Synchronous
- Asynchronous

## Synchronous communication style.

> Services communicate with each other using direct requests

In sync communication when a service is requested data which is stored in another service, the
original service makes a request to the service that contains the data to query its database and
retrieve the information for it.

Benefits on sync comm:

- Conceptually is easy to understand
- That particular service may not need a database

Downsides:

- Introduces dependencies (if any service fails in the chain of requests, overall the main request
  will fail).
- The entire request is only as fast as the slowest request.
- Can introduce an unknown waterfall of requests. (If we don't know if the services needed have
  dependencies of their own)

## Asynchronous communication style.

> Services communicate with each other using _events._ Event-Based Communication

There are two ways of achieving event based communication.

### Single Event Bus

The first one is based on a single event bus, this have very big downsides and not is not great for
scalability. The main goal of this event bus is to handle notification about somethings that happens
on a single service, so that other services can listen to and act on it.

Each service connects to the event bus and can receive or send events to the bus. In this pattern,
every single action in a service is shared to the others interested in listening to it. This event
bus is loaded with event labeled with types and payloads. There are several downsides to it because
it shares all the downsides with the sync communication and even has more added because of the
complexity to create and manage the bus. But the single chain of requests still exist. If one
service is down then the other that is dependent on the data that had to be provided by the offline
service, then the entire request will fail. Dependency is still hard here

The second way of achieving is a variation of the event bus.

### Essential Data duplication in each service own database through event response.

The second pattern seems bizarre and inefficient at first, but it achieves the main issue with data
sharing, all services become independent from each other, there are no single points of failure for
the whole app and if a particular service fails, then others dependant on its data won't be affected
because the requested functional service will have the essential data it needs to operate normally.

#### Notes and concerns:

- The idea of an event bus may be in conflict with my idea of immutable data and pure functions.
  This events are absolutely not pure and may lead to unexpected bugs and behavior.
- A solution for this is to use streams of data. (RxJS maybe?)
- Will it be a good idea to refactor a certainly convoluted and monolithic app to this?.
- I won't deny that the idea of running a service as a containerized app is appealing to me.
  Although I've seen detractors of container technology, it is having rapidly grow in the industry
  as I may have heard.
