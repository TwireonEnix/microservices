const R = require('ramda');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

const posts = [];

app.get('/posts', ({}, res) => res.send(posts));

app.post('/events', ({ body }, res) => {
  const { type, data } = body;
  switch (type) {
    case 'PostCreated':
      posts.push({ ...data, comments: [] });
      break;
    case 'CommentCreated':
      const { postId, ...commentData } = data;
      const ind = R.findIndex(R.propEq('id', postId), posts);
      posts[ind].comments.push(commentData);

      break;
  }
  console.dir(posts, { depth: null });
  res.status(204).end();
});

app.listen(3300, console.log('Query on 3300'));
