const express = require('express');
const { randomBytes } = require('crypto');
const cors = require('cors');
const morgan = require('morgan');
const { default: axios } = require('axios');

const app = express();

// in this toy project we wont be saving anything in a database.
const posts = [];

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

app
  .route('/posts')
  .get((req, res) => res.send(posts))
  .post(async ({ body }, res) => {
    const id = randomBytes(4).toString('hex');
    const { title } = body;
    const created = { id, title };
    // mutation everywhere, also memory leak. Im aware, but in this toy project wont matter :/
    posts.push(created);

    /** Here we will notify the event bus about what just happened. The event object will have two base properties
     * a type and a data payload. The data payload will be
     */
    await axios.post('http://localhost:3400/events', {
      type: 'PostCreated',
      data: created
    });

    res.status(201).send({ msg: 'Post created', created });
  });

app.post('/events', ({ body: { type } }, res) => {
  console.log({ message: 'Event received', type });
  res.status(204).end();
});

app.listen(3100, console.log('Posts on 3100'));
