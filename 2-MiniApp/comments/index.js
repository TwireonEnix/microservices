const express = require('express');
const { randomBytes } = require('crypto');
const cors = require('cors');
const morgan = require('morgan');
const { default: axios } = require('axios');

const app = express();

// in this toy project we wont be saving anything in a database.
const commentsPostById = {};

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

app
  .route('/posts/:id/comments')
  .get(({ params: { id } }, res) => res.send(commentsPostById[id] || []))
  .post(async ({ body: { content }, params: { id } }, res) => {
    const commentId = randomBytes(4).toString('hex');
    const created = { id: commentId, content };

    // i would never do this in a production setup, i heavily dislike this kind of mutation.
    const comments = commentsPostById[id] || [];
    comments.push(created);
    commentsPostById[id] = comments;

    await axios.post('http://localhost:3400/events', {
      type: 'CommentCreated',
      data: { ...created, postId: id }
    });

    res.status(201).send({ msg: 'Post created', comments });
  });

app.post('/events', ({ body: { type } }, res) => {
  console.log({ message: 'Event received', type });
  res.status(204).end();
});

app.listen(3200, console.log('Comments on 3200'));
