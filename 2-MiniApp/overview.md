# Initial Microservices MiniApp

It is a small app that allows users to create posts and other users to create comments on those
posts. Although the idea is really simple we will encounter a lot of challenges to implement as a
microservice There should be a service for each resource

- Posts

  - Create Posts
  - List all posts

- Comments

  - Create a comment
  - List all comments

However, the listing post is more complex that the diagram shows because it have a direct dependency
with the posts. The comments should be attached to a its own post.

> In this app the architecture of microservices with event-driven pattern and data duplication will
> be implemented

## Client

## Posts

The post microservice only has two small handlers: a get and a post request.

## Problems

The client makes a request for getting comments for every post

### Solution:

Async communication

![](2020-07-15-08-55-09.png)

Using a query service that will listen events from the broker to create a data structure to compress
all the requests into one request.
