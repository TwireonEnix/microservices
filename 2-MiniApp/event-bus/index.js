const express = require('express');
const { default: axios } = require('axios');
const cors = require('cors');
const morgan = require('morgan');

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

app.post('/events', async ({ body: event }, res) => {
  const p1 = axios.post('http://localhost:3100/events', event); // posts service
  const p2 = axios.post('http://localhost:3200/events', event); // comments service
  const p3 = axios.post('http://localhost:3300/events', event); // query service
  await Promise.all([p1, p2, p3]);
  res.status(204).end();
});

app.listen(3400, console.log('Event-bus on 3400'));
