import React from 'react';
import PostCreate from './Posts/PostCreate';
import PostList from './Posts/PostList';

export default () => (
  <div className='container'>
    <h1 className='title'>Create Post</h1>
    <PostCreate />
    <hr />
    <h2 className='subtitle'>Posts</h2>
    <PostList />
  </div>
);
