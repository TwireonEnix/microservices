import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CommentCreate from '../Comments/CommentCreate';
import CommentList from '../Comments/CommentList';

/** useState is a react hook to manage internal state of the component
 * useEffect is used to run particular pieces of code triggered by the lifecycle hooks of the component
 */

export default () => {
  const [posts, setPosts] = useState([]);

  const fetchPosts = async () => {
    const { data } = await axios.get('http://localhost:3300/posts');
    setPosts(data);
  };

  useEffect(() => void fetchPosts(), []);

  return (
    <div className='columns is-desktop is-2'>
      {posts.map(post => (
        <div className='card column' key={post.id}>
          <h6 className='card-content subtitle is-2'>{post.title}</h6>
          <CommentList comments={post.comments} />
          <CommentCreate postId={post.id} />
        </div>
      ))}
    </div>
  );
};
