import React, { useState } from 'react';
import axios from 'axios';

export default () => {
  const [title, setTitle] = useState(''); // 2 way binding? I don't know react but i infer from behavior.

  const createPost = async event => {
    event.preventDefault();
    if (title === '') return;
    await axios.post('http://localhost:3100/posts', { title });
    setTitle('');
  };

  return (
    <div>
      <form onSubmit={createPost}>
        <div className='field'>
          <input
            className='input is-primary'
            placeholder='Title'
            value={title}
            onChange={({ target }) => setTitle(target.value)}
          />
        </div>
        <button className='button is-primary'>Submit</button>
      </form>
    </div>
  );
};
