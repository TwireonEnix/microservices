import React, { useState } from 'react';
import axios from 'axios';

export default ({ postId }) => {
  const [content, setContent] = useState('');

  const submitComment = async e => {
    e.preventDefault();
    if (content === '') return;
    await axios.post(`http://localhost:3200/posts/${postId}/comments`, { content });
    setContent('');
  };

  return (
    <div>
      <form onSubmit={submitComment}>
        <div className='field'>
          <input
            className='input is-info'
            placeholder='New comment'
            value={content}
            onChange={({ target }) => setContent(target.value)}
          />
        </div>
        <button className='button is-info'>Submit</button>
      </form>
    </div>
  );
};
