// import React, { useState, useEffect } from 'react';
import React from 'react';
// import axios from 'axios';

export default ({ comments }) => {
  /** No longer necessary due to calling the query service to fetch the post in the
   * parent component.
   */
  // const [comments, setComments] = useState({});

  // const fetchComments = async () => {
  //   const { data } = await axios.get(`http://localhost:3200/posts/${postId}/comments`);
  //   setComments(data);
  // };

  // useEffect(() => void fetchComments(), []);

  const renderedComments = Object.values(comments).map(comment => (
    <li key={comment.id}>{comment.content}</li>
  ));

  return (
    <div className='content'>
      <ul type='I'>{renderedComments}</ul>
    </div>
  );
};
